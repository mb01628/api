//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
  next();
})
var requestjson = require('request-json');

var path = require('path');

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/adominguez/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

var urlClientesMongo = "https://api.mlab.com/api/1/databases/adominguez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlClientesMongo);

app.listen(port);

var movimientosJSONv2 = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  //res.send("HOla Mundo desde NODE.JS");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function(req, res) {
  res.send("Hemos recibido su petición POST");
})

app.put('/', function(req, res) {
  res.send("Hemos recibido su petición PUT");
})

app.delete('/', function(req, res) {
  res.send("Hemos recibido su petición DELETE");
})

app.get('/Clientes', function(req, res) {
  res.send("Aqui tiene a los clientes");
})

app.get('/Clientes/:idCliente', function(req, res) {
  res.send("Aqui tiene al cliente " + req.params.idCliente);
})

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosJSONv2);
})

app.get('/v2/Movimientos/:id', function(req, res) {
  console.log(req.params.id);
  res.send(movimientosJSONv2[req.params.id -1]);
})
//query
app.get('/v2/Movimientosq', function(req, res) {
  console.log(req.query);
  res.send("Petición con Query recibida y cambiada :" + req.query);
})

app.get('/v2/Movimientos/:id/:nombre', function(req, res) {
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("Recibido");
})
//Obtiene clientes de MongoDB
app.get('/v2/Clientes', function(req, res) {
clienteMLab.get('',function(err,resM, body) {
  if (err) {
    console.log(body); }
  else {
    res.send(body);
  }

})
})
//Inserta clientes en MongoDB a través de MLab
app.post('/v2/Clientes', function(req, res) {
  clienteMLab.post('', req.body, function(err,resM, body) {
    if (err) {
      console.log(body); }
    else {
      res.send(body);
    }
  })
})

//Peticion REST para Login
app.post('/v2/Login', function(req, res) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 var email = req.headers.email;
 var password = req.headers.password;
 var query = 'q={"email":"'+ email + '", "password":"' + password + '"}';
 console.log(query);
 clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
 console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
 clienteMLabRaiz.get('', function(err, resM, body) {
   if (!err) {
     if (body.length > 0) { //Login ok
       res.status(200).send('Usuario logado correctamente');
     } else {
       res.status(404).send('Usuario no encontrado');
     }
   } else {
     console.log(body);
   }
 })
 })
